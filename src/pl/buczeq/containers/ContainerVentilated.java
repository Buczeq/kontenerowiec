package pl.buczeq.containers;

public class ContainerVentilated extends Container {
    private boolean ventilation;

    public ContainerVentilated() {
        this.setType("ContainerVentilated");
        this.setAvailableContents(new String[]{"coffe beans", "cocoa beans", "chestnuts", "Solid feed"});
        this.ventilation = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|ventilation=" + ventilation;
    }

    public boolean isVentilation() {
        return ventilation;
    }

    public void setVentilation(boolean ventilation) {
        this.ventilation = ventilation;
    }
}
