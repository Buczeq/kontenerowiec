package pl.buczeq.containers;

import pl.buczeq.Size;

public class ContainerRefrigerated extends Container {
    private boolean refrigerator;

    public ContainerRefrigerated() {
        this.setType("ContainerRefrigerated");
        this.setAvailableSizes(new Size[]{Size.FT20, Size.FT40});
        this.setAvailableContents(new String[]{"fruits", "vegetables", "ice cream", "drugs", "meat"});
        this.refrigerator = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|refrigerator=" + refrigerator;
    }

    public boolean isRefrigerator() {
        return refrigerator;
    }

    public void setRefrigerator(boolean refrigerator) {
        this.refrigerator = refrigerator;
    }
}
