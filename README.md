# Kontenerowiec / Container Ship

[PL]
Finalna wersja projektu ocenionego na 10/10 punktów.
Projekt został rozbity z wymaganego 1 pliku na poszczególne klasy.

Wymagania obejmowały zakaz używania gotowych kolekcji i struktur danych więc cały projekt oparty jest o tablice.

Uproszczone polecenie:

- Utwórz hierarchię klas opisującą kontenery 
- Wygeneruj listę 15000 losowych kontenerów
- Zapisz listę w pliku tekstowym (Zakaz serializacji)
- Wczytaj z pliku listę kontenerów i ułóż w porządku ładowania z zachowaniem bezpiecznego rozkładu masy
- Zapisz id, pozycję, wagę i towar w pliku

[ENG]
Final version of project graded 10 out of 10 possible points
Project has been separated from 1 required file on individual classes.
The requirements included a ban on the use of ready-made collections and data structures, so the entire project is based on arrays.

Simplified command:

- Create a class hierarchy to describe the containers
- Generate a list of 15,000 random containers
- Save the list in a text file (Without serialization)
- Load the container list from the file and arrange the loading order with safe weight distribution
- Save id, position, weight and commodity in a file
