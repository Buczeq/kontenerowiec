package pl.buczeq;

import pl.buczeq.containers.Container;

public class Cell {
    private Position position;
    private Container[] containers;
    private boolean onTop;
    private boolean highContainer;
    private int freeSpace = 40;
    private String stowage;

    public Cell(Position position, boolean onTop, boolean highContainer) {
        this.position = position;
        this.onTop = onTop;
        this.highContainer = highContainer;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Container[] getContainers() {
        return containers;
    }

    public void setContainers(Container[] containers) {
        this.containers = containers;
    }

    public boolean isOnTop() {
        return onTop;
    }

    public void setOnTop(boolean onTop) {
        this.onTop = onTop;
    }

    public boolean isHighContainer() {
        return highContainer;
    }

    public void setHighContainer(boolean highContainer) {
        this.highContainer = highContainer;
    }

    public int getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(int freeSpace) {
        this.freeSpace = freeSpace;
    }

    public String getStowage() {
        return stowage;
    }

    public void setStowage(String stowage) {
        this.stowage = stowage;
    }
}
