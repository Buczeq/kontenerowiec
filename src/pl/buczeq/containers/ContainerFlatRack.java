package pl.buczeq.containers;

import pl.buczeq.Size;

public class ContainerFlatRack extends ContainerOpenTop {
    private boolean openSides;

    public ContainerFlatRack() {
        this.setType("ContainerFlatRack");
        this.setAvailableSizes(new Size[]{Size.FT10OT, Size.FT20OT, Size.FT40OT});
        this.setAvailableContents(new String[]{"machinery", "pipes", "vehicles", "construction materials", "bulky raw materials"});
        this.setOpenTop(true);
        this.openSides = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|openSides=" + openSides;
    }

    public boolean isOpenSides() {
        return openSides;
    }

    public void setOpenSides(boolean openSides) {
        this.openSides = openSides;
    }
}
