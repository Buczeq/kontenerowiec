package pl.buczeq.containers;

public class ContainerDoubleDoor extends Container {
    private boolean doubleDoor;

    public ContainerDoubleDoor() {
        this.setType("ContainerVentilated");
        this.doubleDoor = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|doubleDoor=" + doubleDoor;
    }

    public boolean isDoubleDoor() {
        return doubleDoor;
    }

    public void setDoubleDoor(boolean doubleDoor) {
        this.doubleDoor = doubleDoor;
    }
}
