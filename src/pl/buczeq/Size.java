package pl.buczeq;

/**
 * 3 długości kontenerów, każdy może być wysoki (High pl.buczeq.container.Container) oraz na szczycie (On Top)
 * 3 pl.buczeq.container lengths, each can be high (HC) or on top (Open top)
 */
public enum Size {
    FT10, FT10HC, FT10OT,
    FT20, FT20HC, FT20OT,
    FT40, FT40HC, FT40OT
}
