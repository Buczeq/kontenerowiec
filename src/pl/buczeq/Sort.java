package pl.buczeq;

import pl.buczeq.containers.Container;

/**
 * Algorytm QuickSort sortujący listę kontenerów po wadze
 * QuickSort algorithm sorting array by weight
 */
public class Sort {
    public static void sortContainersByMass(Container[] containers, int first, int last) {
        if (first < last) {
            int partitionIndex = partition(containers, first, last);
            sortContainersByMass(containers, first, partitionIndex - 1);
            sortContainersByMass(containers, partitionIndex + 1, last);
        }
    }

    private static int partition(Container[] containers, int first, int last) {
        int pivot = containers[last].getMass();
        int i = (first - 1);

        for (int j = first; j < last; j++) {
            if (containers[j].getMass() >= pivot) {
                i++;

                Container swapTemp = containers[i];
                containers[i] = containers[j];
                containers[j] = swapTemp;
            }
        }
        Container swapTemp = containers[i + 1];
        containers[i + 1] = containers[last];
        containers[last] = swapTemp;

        return i + 1;
    }
}
