package pl.buczeq.containers;

import pl.buczeq.Size;

public class ContainerOpenTop extends Container {
    private boolean openTop;

    public ContainerOpenTop() {
        this.setType("ContainerOpenTop");
        this.setAvailableSizes(new Size[]{Size.FT10OT, Size.FT20OT, Size.FT40OT});
        this.setAvailableContents(new String[]{"machinery", "pipes", "cables", "construction materials", "bulky raw materials"});
        this.openTop = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|openTop=" + openTop;
    }

    public boolean isOpenTop() {
        return openTop;
    }

    public void setOpenTop(boolean openTop) {
        this.openTop = openTop;
    }
}
