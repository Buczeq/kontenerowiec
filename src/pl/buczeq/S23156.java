package pl.buczeq;

import pl.buczeq.containers.Container;

/**
 * @author Tomasz Buczyński
 */
public class S23156 {
    public static void main(String[] args) {
        String generatedContainers = ContainerHelper.generateRandomContainers(15000);
        String path = ContainerHelper.saveContainersToFile(generatedContainers);

        Container[] containers = ContainerHelper.readContainersFromFile(path);

        Sort.sortContainersByMass(containers, 0, containers.length-1);

        Ship ship = new Ship();
        ship.loadShip(containers);
//        ship.showStowage();
//        ship.showWeightDifference();

        ContainerHelper.generateManifestFile(ship.loadedContainers);
    }
}

