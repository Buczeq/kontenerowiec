package pl.buczeq.containers;

import pl.buczeq.Cell;
import pl.buczeq.Size;

public class Container {
    private String type = "Container";
    private int mass;
    private Size size;
    private Size[] availableSizes = {Size.FT10, Size.FT10HC,
            Size.FT20, Size.FT20HC,
            Size.FT40, Size.FT40HC};
    private String[] availableContents = {"electronics", "boxes", "pallets", "sacks", "barrels", "clothes"};
    private int id;
    private String content;
    private Cell cell;
    private int idInCell;

    @Override
    public String toString() {
        return "type=" + type +
                "|mass=" + mass +
                "|size=" + size +
                "|id=" + id +
                "|content=" + content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Size[] getAvailableSizes() {
        return availableSizes;
    }

    public void setAvailableSizes(Size[] availableSizes) {
        this.availableSizes = availableSizes;
    }

    public String[] getAvailableContents() {
        return availableContents;
    }

    public void setAvailableContents(String[] availableContents) {
        this.availableContents = availableContents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public int getIdInCell() {
        return idInCell;
    }

    public void setIdInCell(int idInCell) {
        this.idInCell = idInCell;
    }
}
