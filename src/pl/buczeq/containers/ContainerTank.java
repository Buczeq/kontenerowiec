package pl.buczeq.containers;

public class ContainerTank extends Container {
    private boolean tank;

    public ContainerTank() {
        this.setType("ContainerTank");
        this.setAvailableContents(new String[]{"petrol", "diesel", "water", "LPG", "wine", "acid"});
        this.tank = true;
    }

    @Override
    public String toString() {
        return super.toString() +
                "|tank=" + tank;
    }

    public boolean isTank() {
        return tank;
    }

    public void setTank(boolean tank) {
        this.tank = tank;
    }
}
