package pl.buczeq;

import pl.buczeq.containers.*;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ContainerHelper {

    /**
     * Wszystkie możliwe typy kontenerów
     *
     * every possible types of containers
     */
    private static final Class<?>[] containerTypes = new Class[]{
            Container.class,
            ContainerRefrigerated.class,
            ContainerTank.class,
            ContainerVentilated.class,
            ContainerDoubleDoor.class,
            ContainerOpenTop.class,
            ContainerFlatRack.class
    };

    /**
     * Rozmiar warstwy na statku to ~950 komórek.
     * Kontenery ponadgabarytowe są dość wyjątkowe więc ich ilość nie przekracza 1500.
     * 1500 kontenerów mieści się w 950 komórkach.
     * <p>
     * Masa i zawartość kontenera zależy od rozmiaru i jest losowana
     *
     * pl.buczeq.Size of layer on ship is about ~950 cells.
     * Containers oversize are quite exceptional so their amount doesn't exceed 1500.
     * 15000 containers fit in 950 cells
     * Mass and content of pl.buczeq.container depends on size and is being drawn
     *
     * @param amount liczba kontenerów do wygenerowania
     *               amount of containers to drawn
     * @return kontenery, każdy w nowym wierszu
     *         containers, each in new line
     */
    public static String generateRandomContainers(int amount) {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        int onTopCounter = 0;
        for (int i = 0; i < amount; i++) {
            try {
                Class<?> clazz = containerTypes[random.nextInt(containerTypes.length)];
                while (onTopCounter > 1500 &&
                        (clazz.equals(ContainerOpenTop.class) || clazz.equals(ContainerFlatRack.class))) {
                    clazz = containerTypes[random.nextInt(containerTypes.length)];
                }

                Container container = ((Container) clazz.getDeclaredConstructor().newInstance());
                container.setSize(container.getAvailableSizes()[random.nextInt(container.getAvailableSizes().length)]);
                container.setId(i);
                int massRatio = Integer.parseInt(container.getSize().toString().substring(2, 4)) / 10;
                container.setMass(random.nextInt((10000 * massRatio) - (8000 * massRatio)) + (8000 * massRatio));
                container.setContent(container.getAvailableContents()[random.nextInt(container.getAvailableContents().length)]);

                if (container.getSize().toString().endsWith("OT")) onTopCounter++;

                builder.append(container.toString()).append("\n");
            } catch (Exception ex) {
                System.out.println("Exception with generating random Containers");
            }
        }
        return builder.toString();
    }

    public static String saveContainersToFile(String containers) {
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String path = "./generatedContainers" + date + ".txt";

        return saveStringToFile(containers, path);
    }

    private static String saveStringToFile(String data, String path) {
        try {
            byte[] dataBytes = data.getBytes();

            RandomAccessFile stream = new RandomAccessFile(path, "rw");
            FileChannel channel = stream.getChannel();

            ByteBuffer buffer = ByteBuffer.allocate(dataBytes.length);
            buffer.put(dataBytes);
            buffer.flip();
            channel.write(buffer);
            channel.close();
            stream.close();
        } catch (IOException ex) {
            System.out.println("Błąd podczas zapisywania kontenerów do pliku \n" +
                    "Sprawdź ścieżkę do pliku");
        }
        return path;
    }

    public static Container[] readContainersFromFile(String path) {
        try {
            RandomAccessFile reader = new RandomAccessFile(path, "r");
            FileChannel channel = reader.getChannel();
            //average file size with 15k containers has about 1,200,000 bytes
            int bufferSize = 10000000;
            if (bufferSize > channel.size()) {
                bufferSize = (int) channel.size();
            }
            ByteBuffer buff = ByteBuffer.allocate(bufferSize);
            channel.read(buff);
            buff.flip();

            String containersInString = new String(buff.array());
            channel.close();
            reader.close();
            return stringToContainers(containersInString);
        } catch (IOException ex) {
            System.out.println("Exception while reading file. Check path.");
            System.out.println(path);
        }
        return null;
    }

    private static Container[] stringToContainers(String containersInString) {
        String[] lines = containersInString.split("\n");
        Container[] containers = new Container[lines.length];
        int index = 0;

        for (String line : lines) {
            String[] typesAndValues = line.split("\\|");

            String[] values = new String[typesAndValues.length];
            for (int i = 0; i < typesAndValues.length; i++) {
                String[] typeAndValue = typesAndValues[i].split("=");
                values[i] = typeAndValue[1];
            }
            try {
                Container container = (Container) Class.forName("pl.buczeq.containers." + values[0]).getDeclaredConstructor().newInstance();
                container.setMass(Integer.parseInt(values[1]));
                container.setSize(Size.valueOf(values[2]));
                container.setId(Integer.parseInt(values[3]));
                container.setContent(values[4]);

                containers[index++] = container;
            } catch (Exception ex) {
                System.out.println("Exception with creating Container: " + values[0]);
            }
        }
        return containers;
    }

    public static void generateManifestFile(Container[] containers) {
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String path = "./manifest" + date + ".txt";

        StringBuilder builder = new StringBuilder();
        builder.append("ID\t").append("POKLAD/WARSTWA-RZAD-WNEKA/NR\t").append("WAGA\t").append("TOWAR\n");
        for (Container container : containers) {
            builder.append(container.getId()).append("\t")
                    .append(container.getCell().getStowage()).append("/")
                    .append(container.getCell().getPosition().getLayer()).append("-")
                    .append(container.getCell().getPosition().getRow()).append("-")
                    .append(container.getCell().getPosition().getBay()).append("/")
                    .append(container.getIdInCell()).append("\t")
                    .append(container.getMass()).append("\t")
                    .append(container.getContent()).append("\n");

        }
        saveStringToFile(builder.toString(), path);
    }

}
