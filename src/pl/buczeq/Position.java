package pl.buczeq;

public class Position {
    private int bay;
    private int row;
    private int layer;

    public Position(int layer, int row, int bay) {
        this.bay = bay;
        this.row = row;
        this.layer = layer;
    }

    public int getBay() {
        return bay;
    }

    public void setBay(int bay) {
        this.bay = bay;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }
}
