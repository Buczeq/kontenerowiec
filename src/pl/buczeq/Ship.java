package pl.buczeq;

import pl.buczeq.containers.Container;

/**
 * Statek jest podzielony na warstwy, rzędy, wnęki oraz komórki
 * Każda komórka jest długa na 40FT i jest w stanie pomieścić 4 kontenery 10FT, 2 * 20FT etc.
 *
 * pl.buczeq.Ship is divided on layers, rows, bays and cells
 * Each cell is 40FT long and can contain 4 containers 10FT, 2 *20FT etc.
 */
public class Ship {
    public int amountOfBays = 80;
    public int amountOfLayers = 8;
    public int amountOfRows = 12;

    public Cell[][][] upStowage = new Cell[amountOfLayers][amountOfRows][amountOfBays];
    public Cell[][][] downStowage = {new Cell[4][amountOfBays],
            new Cell[6][amountOfBays],
            new Cell[6][amountOfBays],
            new Cell[8][amountOfBays],
            new Cell[8][amountOfBays]};
    public boolean isDownStowageFullFilled = false;
    public Container[] loadedContainers = new Container[0];

    /**
     * Ladowanie statku w 2 etapach.
     * Etap 1: Ladowaine wszystkich kontenerów oprócz 'ONTOP'
     * Etap 2: Ladowanie kontenerów 'ONTOP'
     *
     *
     * Loading ship in 2 steps
     *
     * Step 1: Loading every containers except 'ONTOP'
     * Step 2: Loading 'ONTOP' containers
     *
     * @param containers tablica kontenerów posortowana wagowo
     *                   containers array sorted by weight
     */
    public void loadShip(Container[] containers) {
        for (Container container : containers) {
            if (!container.getSize().toString().endsWith("OT")) {
                if (!isDownStowageFullFilled) {
                    isDownStowageFullFilled = checkIfDownStowageIsFullFilled();
                }
                addContainerToShip(container, isDownStowageFullFilled);
            }
        }
        for (Container container : containers) {
            if (container.getSize().toString().endsWith("OT")) {
                if (!isDownStowageFullFilled) {
                    isDownStowageFullFilled = checkIfDownStowageIsFullFilled();
                }
                addContainerToShip(container, isDownStowageFullFilled);
            }
        }
    }

    /**
     * Sprawdzanie czy dolny pokład jest pełen
     *
     * check if down stowage is full
     */
    private boolean checkIfDownStowageIsFullFilled() {
        //check only last row on last layer
        for (Cell cell : downStowage[4][downStowage[4].length - 1]) {
            if (cell == null || cell.getFreeSpace() != 0)
                return false;
        }
        return true;
    }

    /**
     * Dla każdego kontenera wyszukuje pierwszą możliwą komórkę.
     * Rzędy w kontenerowcu liczone są od środka więc należy iterować od 0
     * 6 4 2 0 1 3 5 7 etc
     *
     * For each pl.buczeq.container search first available cell
     * Rows in pl.buczeq.container ship are counted from center so we need to iterate from 0
     * 6 4 2 0 1 3 5 7 etc
     *
     * @param container kontener
     */
    private void addContainerToShip(Container container, boolean isDownStowageFullFilled) {
        Cell[][][][] stowages = {downStowage, upStowage};
        outerloop:
        for (Cell[][][] stowage : stowages) {
            if (isDownStowageFullFilled) {
                stowage = upStowage;
            }
            for (int l = 0; l < stowage.length; l++) {
                for (int r = 0; r < stowage[l].length; r++) {
                    for (int b = 0; b < stowage[l][r].length; b++) {
                        Position position = new Position(l, r, b);
                        if (canAddContainer(container, stowage[l][r][b]) && canLoadOnCellBelow(stowage, position)) {
                            if (stowage[l][r][b] == null) {
                                stowage[l][r][b] =
                                        new Cell(position,
                                                container.getSize().toString().endsWith("OT"),
                                                container.getSize().toString().endsWith("HC"));
                                stowage[l][r][b].setContainers(new Container[0]);
                            }
                            addContainerToCell(container, stowage[l][r][b]);
                            loadedContainers = addContainerToArray(loadedContainers, container);
                            if (!isDownStowageFullFilled) {
                                container.getCell().setStowage("down");
                                downStowage = stowage;
                            } else {
                                container.getCell().setStowage("up");
                                upStowage = stowage;
                            }
                            break outerloop;
                        }
                    }
                }
            }
        }
    }

    private boolean canLoadOnCellBelow(Cell[][][] stowage, Position position) {
        try {
            Cell cellBelow = stowage[position.getLayer() - 1][position.getRow()][position.getBay()];
            if (cellBelow == null || cellBelow.isOnTop()) {
                return false;
            }
        } catch (IndexOutOfBoundsException ex) {
            return true;
        }
        return true;
    }

    /**
     * Sprawdzenie czy typ komórki odpowiada typowi kontenera
     * Sprawdzenie czy jest wystarczająca ilość miejsca
     *
     * Check if cell type equal pl.buczeq.container type
     * Check if there is enough space for pl.buczeq.container
     */
    public boolean canAddContainer(Container container, Cell cell) {
        if (cell == null) return true;
        int length = Integer.parseInt(container.getSize().toString().substring(2, 4));
        boolean onTop = container.getSize().toString().endsWith("OT");
        boolean highContainer = container.getSize().toString().endsWith("HC");
        if (cell.getFreeSpace() - length >= 0) {
            return onTop && cell.isOnTop() || highContainer && cell.isHighContainer() || !onTop && !cell.isOnTop();
        }
        return false;
    }

    private void addContainerToCell(Container container, Cell cell) {
        cell.setContainers(addContainerToArray(cell.getContainers(), container));
        container.setIdInCell(cell.getContainers().length);
        cell.setFreeSpace(subtractSpaceFromCell(container, cell));
        container.setCell(cell);
    }

    protected int subtractSpaceFromCell(Container container, Cell cell) {
        int length = Integer.parseInt(container.getSize().toString().substring(2, 4));
        return cell.getFreeSpace() - length;
    }

    private Container[] addContainerToArray(Container[] containers, Container container) {
        Container[] temp = new Container[containers.length + 1];
        System.arraycopy(containers, 0, temp, 0, containers.length);
        temp[containers.length] = container;
        return temp;
    }

    /**
     * Rzędy w kontenerowcu liczone są od środka więc licząc stosunek wag lewo/prawo
     * należy dzielić rzędy na parzyste i nieparzyste
     * 6 4 2 0 1 3 5 7 etc.
     *
     * Rows in pl.buczeq.container ship are counted from center so when calculating weight left/right
     * we need to divide rows by even and odd
     * 6 4 2 0 1 3 5 7 etc.
     */
    public void showWeightDifference() {
        Cell[][][][] stowages = {downStowage, upStowage};
        long left = 0;
        long right = 0;
        long front = 0;
        long back = 0;
        for (Cell[][][] stowage : stowages) {
            for (Cell[][] cells : stowage) {
                for (int r = 0; r < cells.length; r++) {
                    for (int b = 0; b < cells[r].length; b++) {
                        if (cells[r][b] != null) {
                            for (Container container : cells[r][b].getContainers()) {
                                if (r % 2 == 0) right += container.getMass();
                                else left += container.getMass();
                                if (b > amountOfBays / 2) front += container.getMass();
                                else back += container.getMass();

                            }
                        }
                    }
                }
            }
        }

        System.out.println("left/right difference: " + (((right - left) * 100) / right) + "%");
        System.out.println("front/back difference: " + (((front - back) * 100) / front) + "%");
    }

    /**
     * Wyświetla pokłady w sposób:
     * Warstwa{
     * rząd{wnęka, wnęka...}
     * rząd{wnęka, wnęka...}
     * }
     *
     * Printing stowages like:
     * Layer{
     *     row{bay, bay...}
     *     row{bay, bay...}
     * }
     */
    public void showStowage() {
        Cell[][][] stowage = upStowage;
        System.out.print("Stowage:\n");
        for (Cell[][] cells : stowage) {
            for (int j = 0; j < cells.length; j++) {
                for (int k = 0; k < cells[j].length; k++) {
                    if (cells[j][k] != null) {
                        System.out.print(cells[j][k].isOnTop());

                        System.out.print("-|");
                    } else {
                        System.out.print("empty");
                    }
                    if (k == amountOfBays - 1 && j != cells.length - 1) {
                        System.out.print("\n");
                    }
                    if (k == amountOfBays - 1 && j == cells.length - 1) {
                        System.out.print("\n");
                        System.out.print("\n");
                    }
                }
            }
        }

    }

}
